
Early work on the Pledgebank.com module.

This module is *not* finished, and therefore should not 
be used in production.

Current status:  Can correctly retrieve pledge details, and store them, and create pledgebank nodes.
Does not currently insert the list of signers, nor render the pledge correctly.

Watch this space.
